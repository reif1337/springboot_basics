package studentWithoutDB;

import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

//@Component
@Service
public class StudentService {
    public List<studentWithoutDB.Student>  getStudents() {
        return List.of(
                new Student(1L, "Franz", "franz.reichel@htlstp.ac.at", LocalDate.parse("1983-01-26"), 40),
                new Student(2L, "Werner", "werner.gitschthaler@htlstp.ac.at", LocalDate.parse("1981-01-26"), 42)
        );
    }
}
