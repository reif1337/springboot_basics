package studentWithoutDB;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping(path = "api/v1/student")
public class StudentController {

    /* * /

    // Auslagerung in Services
    @GetMapping
    public List<studentWithoutDB.Student> getStudents() {
        return List.of(
                new Student(1L, "Franz", "franz.reichel@htlstp.ac.at", LocalDate.parse("1983-01-26"), 40),
                new Student(2L, "Werner", "werner.gitschthaler@htlstp.ac.at", LocalDate.parse("1981-01-26"), 42)
        );
    }
    /* */


    /* */

    //Benutzen von StudentService durch Dependency Injection
    private final StudentService studentService;

//    public StudenController() {
//        this.studentService = new StudentService();  // Vermeiden Dependency Injection
//    }

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping
    public List<Student> getStudents() {
        return studentService.getStudents();
    }


    /*  */

}
