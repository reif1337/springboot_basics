package restControllerAndMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
//@RequestMapping("/mapping")
public class MappingController {

    // ********************************************************
    // by Path
    @RequestMapping(value = "/ex/foos", method = GET)
    @ResponseBody
    public String getFoosBySimplePath() {
        return "Get some Foos";
    }
    // curl -i http://localhost:8080/ex/foos
    // curl -i -X GET http://localhost:8080/ex/foos

    // ********************************************************
    // the HTTP Method
    @RequestMapping(value = "/ex/foos", method = POST)
    @ResponseBody
    public String postFoos() {
        return "Post some Foos";
    }
    // curl -i -X POST http://localhost:8080/ex/foos


    // ********************************************************
    // PathVariables

    @RequestMapping(value = "/ex/foos/{fooid}/bar/{barid}", method = GET)
    @ResponseBody
    public String getFoosBySimplePathWithPathVariables
            (@PathVariable long fooid, @PathVariable long barid) {
        return "Get a specific Bar with id=" + barid +
                " from a Foo with id=" + fooid;
    }


    // ********************************************************
    //PathVariable With Regex

    @RequestMapping(value = "/ex/bars/{numericId:[\\d]+}", method = GET)
    @ResponseBody
    public String getBarsBySimplePathWithPathVariable(
            @PathVariable long numericId) {
        return "Get a specific Bar with id=" + numericId;
    }

    //http://localhost:8080/ex/bars/1
    //http://localhost:8080/ex/bars/abc



    // ********************************************************
    // RequestMapping With Request Parameters
    @RequestMapping(value = "/ex/bars", method = GET)
    @ResponseBody
    public String getBarBySimplePathWithRequestParam(
            @RequestParam("id") long id) {
        return "_Get a specific Bar with id=" + id;
    }
    //http://localhost:8080/ex/bars?id=100
    //curl -i -d id=100 http://localhost:8080/ex/bars



    @RequestMapping(value = "/ex/bars2", params = "id", method = GET)
    @ResponseBody
    public String getBarBySimplePathWithExplicitRequestParam(
            @RequestParam("id") long id) {
        return "Get a specific Bar with id=" + id;
    }
    //http://localhost:8080/ex/bars?id=100


    @RequestMapping(
            value = "/ex/bars",
            params = { "id", "second" },
            method = GET)
    @ResponseBody
    public String getBarBySimplePathWithExplicitRequestParams(
            @RequestParam("id") long id) {
        return "Narrow Get a specific Bar with id=" + id;
    }

    // http://localhost:8080/ex/bars?id=100&second=Wuhuu
}
