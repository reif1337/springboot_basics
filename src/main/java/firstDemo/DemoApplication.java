package firstDemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@SpringBootApplication
@RestController
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	//@GetMapping
	//public String hello() {
	//	return "My first Spring Boot Rest Endpoint";
	//}

	@GetMapping("/")
	public String test() {
		return "Ich bin der Franz";
	}

	@GetMapping("/hello")
	public List<String> hello() {
		return List.of("My", "first", "Spring", "Boot", "Rest", "Endpoint");
	}
}

//http://localhost:8080/
//http://localhost:8080/hello
