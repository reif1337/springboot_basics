package demoCustomCode;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Random;

@SpringBootApplication
@RestController
public class CustomCode implements CommandLineRunner {
	public static void main(String[] args) {
		SpringApplication.run(CustomCode.class, args);
	}

	@GetMapping()
	public List<String> hello() {
		return List.of("My first Spring", "Boot Rest Endpoint");
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("My Custom Code Random-number: " + new Random().nextInt(100));
	}
}

//http://localhost:8080/
