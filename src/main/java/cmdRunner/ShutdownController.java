package cmdRunner;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// POST Request
//curl -X POST localhost:8080/shutdownContext

@RestController
@RequestMapping("/shutdownContext")
public class ShutdownController implements ApplicationContextAware {

    private ApplicationContext context;

    @PostMapping
    public ResponseEntity<String> shutdownContext() {

        System.out.println("Entry Thread Id (Debug): " + Thread.currentThread().getName());
        Runnable runnable= () -> {
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {

                System.out.println("Thread was Interrupted! Error in Thread Sleep (2 Seconds!)");
            }
            System.out.println("Callable Thread Id: " + Thread.currentThread().getName());
            ((ConfigurableApplicationContext) context).close();
        };

        new Thread(runnable).start();
        System.out.println("Exit Thread Id (Debug): " + Thread.currentThread().getName());
        return new ResponseEntity<>("Shutdown Requested - Will Shutdown in Next 2 Seconds!", HttpStatus.OK);


    }

    @Override
    public void setApplicationContext(ApplicationContext ctx) throws BeansException {
        this.context = ctx;

    }
}
